EDITION du 20/05/2022

Naball Silver v.0.6.6 - délivré le 01/07/2017

Créé par l'équipe Piwel Engine
 - Alan BRETELLE (game design, programmeur, compositeur musical)
 - Guillaume Jaouen (tests, graphiste)
 - Nicolas Brisset (character designer)

-----------------------------------------------------------------------------------------------------------------
IMPORTANT : L'oeuvre originale a été rendu public le 20/05/2022. Par cette acte, vous êtes autorisé à executer le
jeu et lire et éditer le code source du jeu et ses ressources. Toute re-publication, à but commerciale ou 
non-lucrative, sans accord de l'auteur Alan Bretelle (bretellealan@hotmail.fr), constitue un délit de la 
propriété intellectuelle. 
-----------------------------------------------------------------------------------------------------------------

INFORMATIONS :

Le jeu a été développé avec Blender Game Engine, un ancien moteur de jeu vidéo aujourd'hui disparu.
Il est composé de multiples niveaux au notre personnage, Naball, doit évoluer en collectant des boules d'énergie
et obtenir des nouveaux pouvoirs.

Le démarrage du jeu est à faire avec Naball.exe mais il est possible d'ouvrir le jeu avec une vieille version de 
Blender. Pour cela, il faut ouvrir le fichier MAP_DATAS.scenes du dossier Assets avec Blender. Vous pourrez 
ainsi modifier le code du jeu, la logique, les niveaux etc.


Pour mes amis développeurs : beaucoup de scripts ont été très mal codés. Il s'agit de lignes de codes écrites
quand j'avais 15-16 ans. Il y aussi pas mal de codes morts et de proto-types qui n'ont jamais abouties à des 
vrais features. Enfin, la documentation de la bibliothèque "bge" pourrait ne plus exister aujourd'hui.

